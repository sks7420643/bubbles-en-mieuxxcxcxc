package fr.iut.rodez.bubbles.fx.commons;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public final class Backgrounds {

    private static final BackgroundFill GLASS_FILL = new BackgroundFill(Paints.GLASS, CornerRadii.EMPTY, Insets.EMPTY);
    public static final Background GLASS = new Background(GLASS_FILL);

    private static final BackgroundFill TRANSPARENT_FILL = new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY);
    public static final Background TRANSPARENT = new Background(TRANSPARENT_FILL);

    private Backgrounds() {}
}
