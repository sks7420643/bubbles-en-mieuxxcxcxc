package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;

import java.io.File;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;

public final class FamilyService {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(SNAKE_CASE);
    }

    private FamilyService() {}

    public static FamilyLoadingResult loadFamilyFromFile(File file) {
        try {
            FamilyFile family = objectMapper.readValue(file, FamilyFile.class);
            Set<FamilyMember> members = loadFamilyMembers(family);
            return new FamilyLoadingResult.Loaded(new Family(family.name(), members));
        } catch (Exception e) {
            return new FamilyLoadingResult.LoadingError(e);
        }
    }

    private static Set<FamilyMember> loadFamilyMembers(FamilyFile familyFile) {
        Set<FamilyMember> loadedMembers = familyFile.members()
                                                    .stream()
                                                    .map(FamilyService::toFamilyMember)
                                                    .collect(Collectors.toSet());

        familyFile.relations()
                  .forEach(relation -> createRelation(relation, loadedMembers));

        return loadedMembers;
    }

    private static FamilyMember toFamilyMember(FamilyFile.Member member) {
        FamilyFile.Member.Position fileMemberPosition = member.position();
        Position position = new Position(fileMemberPosition.x(), fileMemberPosition.y());
        Identity identity = new Identity(member.id(), member.name(), member.picture());
        return new FamilyMember(identity, position);
    }

    private static void createRelation(FamilyFile.Relation relation, Set<FamilyMember> members) {
        findById(members, relation.from()).ifPresent(from -> findById(members, relation.to()).ifPresent(to -> {
            switch (relation.type()) {
                case PARENT -> from.parentOf(to);
            }
        }));
    }

    private static Optional<FamilyMember> findById(Set<FamilyMember> members, UUID id) {
        return members.stream()
                      .filter(member -> id.equals(member.identity.id()))
                      .findFirst();
    }

    public sealed interface FamilyLoadingResult {
        record Loaded(Family family) implements FamilyLoadingResult {}

        record LoadingError(Exception cause) implements FamilyLoadingResult {}
    }
}
