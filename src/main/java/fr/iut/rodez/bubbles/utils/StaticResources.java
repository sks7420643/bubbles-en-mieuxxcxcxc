package fr.iut.rodez.bubbles.utils;

import fr.iut.rodez.bubbles.Main;

import java.io.InputStream;
import java.net.URL;

public final class StaticResources {

    public static InputStream loadResourceAsStream(String resourcePath) {
        InputStream resourceAsStream = Main.class.getResourceAsStream(resourcePath);
        if (resourceAsStream == null) {
            throw new IllegalArgumentException("Resource not found: " + resourcePath);
        }
        return resourceAsStream;
    }

    public static URL retrieveResourceURL(String resourcePath) {
        return Main.class.getResource(resourcePath);
    }
}
